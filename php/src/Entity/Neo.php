<?php

namespace App\Entity;

use App\Repository\NeoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Neo
 * @package App\Entity
 */
class Neo
{
    /**
     * @var int|null
     */
    private $id;

    /** @var \DateTimeInterface */
    private $date;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $speed;

    /**
     * @var int
     */
    private $reference_id;

    /**
     * @var bool
     */
    private $is_hazardous;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return Neo
     */
    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Neo
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param $speed
     * @return Neo
     */
    public function setSpeed($speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getReferenceId(): ?int
    {
        return $this->reference_id;
    }

    /**
     * @param int $reference_id
     * @return Neo
     */
    public function setReferenceId(int $reference_id): self
    {
        $this->reference_id = $reference_id;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsHazardous()
    {
        return $this->is_hazardous;
    }

    /**
     * @param $is_hazardous
     * @return Neo
     */
    public function setIsHazardous($is_hazardous): self
    {
        $this->is_hazardous = $is_hazardous;

        return $this;
    }
}
