<?php

namespace App\Controller;

use App\Entity\Neo;
use App\Repository\NeoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NeoController
 * @package App\Controller
 */
class NeoController extends AbstractController
{
    /**
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function hazardous(PaginatorInterface $paginator, EntityManagerInterface $entityManager, Request $request)
    {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $entityManager
            ->getRepository(Neo::class);

        $result = $neoRepository->getHazardous();

        $pagination = $paginator->paginate(
            $result,
            $request->get('page', 1),
            $request->get('limit', 5)
        );

        return $this->json([
            'page' => $pagination->getCurrentPageNumber(),
            'limit' => $pagination->getItemNumberPerPage(),
            'items' => $pagination->getItems(),
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function fastest(EntityManagerInterface $entityManager, Request $request)
    {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $entityManager
            ->getRepository(Neo::class);

        $isHazardous = filter_var($request->get('hazardous', false), FILTER_VALIDATE_BOOLEAN);

        $result =  $neoRepository->getFastest($isHazardous);

        return $this->json($result);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function bestMonth(EntityManagerInterface $entityManager, Request $request)
    {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $entityManager
            ->getRepository(Neo::class);

        $isHazardous = filter_var($request->get('hazardous', false), FILTER_VALIDATE_BOOLEAN);

        $result =  $neoRepository->getBestMonth($isHazardous);

        return $this->json($result);
    }
}
