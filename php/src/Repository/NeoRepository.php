<?php

namespace App\Repository;

use App\Entity\Neo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Neo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Neo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Neo[]    findAll()
 * @method Neo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NeoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Neo::class);
    }

    /**
     * @return int|mixed|string
     */
    public function getHazardous()
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.is_hazardous = :is_hazardous')
            ->setParameter('is_hazardous', 1)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param bool $isHazardous
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFastest(bool $isHazardous)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.is_hazardous = :is_hazardous')
            ->setParameter('is_hazardous', $isHazardous)
            ->setMaxResults(1)
            ->orderBy('n.speed', ' DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param bool $isHazardous
     * @return array|int|string
     */
    public function getBestMonth(bool $isHazardous)
    {
        return $this->createQueryBuilder('n')
            ->select('count(n.id) as count', 'YEAR(n.date) as year', 'MONTH(n.date) as month')
            ->andWhere('n.is_hazardous = :is_hazardous')
            ->setParameter('is_hazardous', $isHazardous)
            ->orderBy('count', 'desc')
            ->setMaxResults(1)
            ->groupBy('year', 'month')
            ->getQuery()
            ->getArrayResult();
    }
}
