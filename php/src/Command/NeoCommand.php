<?php

namespace App\Command;

use App\Entity\Neo;
use App\Services\Neo\NeoDTO;
use App\Services\Neo\NeoInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class NeoCommand extends Command
{
    protected static $defaultName = 'neo:feed';

    /**
     * @var NeoInterface
     */
    private $neoService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * NeoCommand constructor.
     * @param NeoInterface $neoService
     */
    public function __construct(NeoInterface $neoService, EntityManagerInterface $entityManager)
    {
        $this->neoService = $neoService;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Grab entities from NASA NEOs API');
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $start = date_create('now -1 year');
        $end = date_create('now');

        $endDate = clone $end;

        $end = clone $start;
        $end->add(date_interval_create_from_date_string('2 days'));

        do {
            $neos = $this->neoService->get($start, $end);

            $neoRepository = $this->entityManager->getRepository(Neo::class);

            foreach ($neos as $neoDto) {
                /** @var Neo|null $neoEntity */
                $neoEntity = $neoRepository->findOneBy(['reference_id' => $neoDto->getReferenceId()]);

                if (!$neoEntity) {
                    $neoEntity = new Neo();
                }

                $neoEntity->setReferenceId($neoDto->getReferenceId());
                $neoEntity->setIsHazardous($neoDto->getIsHazardous());
                $neoEntity->setSpeed($neoDto->getSpeed());
                $neoEntity->setName($neoDto->getName());
                $neoEntity->setDate($neoDto->getDate());

                $this->entityManager->persist($neoEntity);
            }

            $end->add(date_interval_create_from_date_string('2 days'));

            $this->entityManager->flush();
        } while ($end < $endDate);

        return 0;
    }
}
