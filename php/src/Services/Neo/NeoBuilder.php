<?php

namespace App\Services\Neo;

use mysql_xdevapi\Exception;
use Psr\Log\LoggerInterface;

/**
 * Class NeoBuilder
 * @package App\Services\Neo
 */
class NeoBuilder
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array|null
     */
    private $data;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getNeos(): array
    {
        $result = [];

        foreach ($this->data['near_earth_objects'] as $date => $neos) {
            $date = date_create_from_format('Y-m-d', $date);

            foreach ($neos as $neo) {
                try {
                    $result[] = $this->getNeo($date, $neo);
                } catch (Exception $exception) {
                    $this->logger->critical($exception->getMessage(), [$neo]);
                }
            }
        }

        return $result;
    }

    /**
     * @param \DateTimeInterface $dateTime
     * @param $data
     * @return NeoDTO
     */
    protected function getNeo(\DateTimeInterface $dateTime, $data): NeoDTO
    {
        return (new NeoDTO())
            ->setDate($dateTime)
            ->setName($data['name'])
            ->setSpeed($data['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'])
            ->setIsHazardous($data['is_potentially_hazardous_asteroid'])
            ->setReferenceId($data['neo_reference_id']);
    }
}