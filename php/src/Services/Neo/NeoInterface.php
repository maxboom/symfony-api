<?php

namespace App\Services\Neo;

/**
 * Interface NeoInterface
 * @package App\Services\Neo
 */
interface NeoInterface
{
    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return NeoDTO[]
     */
    public function get(\DateTime $start, \DateTime $end): array;
}