<?php

namespace App\Services\Neo;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class NeoService
 * @package App\Services\Neo
 */
class NeoService implements NeoInterface
{
    private const FEED_URL = 'https://api.nasa.gov/neo/rest/v1/feed';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var NeoBuilder
     */
    private $neoBuilder;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * NeoService constructor.
     * @param HttpClientInterface $httpClient
     * @param string $apiKey
     */
    public function __construct(HttpClientInterface $httpClient, NeoBuilder $neoBuilder, string $apiKey)
    {
        $this->httpClient = $httpClient;
        $this->neoBuilder = $neoBuilder;
        $this->apiKey = $apiKey;
    }

    /**
     * {@inheritDoc}
     */
    public function get(\DateTime $start, \DateTime $end): array
    {
        $response = $this->httpClient->request('GET', self::FEED_URL, [
            'query' => [
                'start_date' => $start->format('Y-m-d'),
                'end_date' => $end->format('Y-m-d'),
                'api_key' => $this->apiKey,
            ],
        ]);

        try {
            $neos = json_decode($response->getContent(), true);
        } catch (\Exception $exception) {
            return [];
        }

        $this->neoBuilder->setData($neos);

        return $this->neoBuilder->getNeos();
    }
}
