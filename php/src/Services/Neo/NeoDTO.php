<?php

namespace App\Services\Neo;

/**
 * Class NeoDTO
 * @package App\Services\Neo
 */
class NeoDTO
{
    /** @var \DateTimeInterface */
    private $date;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $speed;

    /**
     * @var int
     */
    private $reference_id;

    /**
     * @var bool
     */
    private $is_hazardous;

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return NeoDTO
     */
    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return NeoDTO
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param $speed
     * @return NeoDTO
     */
    public function setSpeed($speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getReferenceId(): ?int
    {
        return $this->reference_id;
    }

    /**
     * @param int $reference_id
     * @return NeoDTO
     */
    public function setReferenceId(int $reference_id): self
    {
        $this->reference_id = $reference_id;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsHazardous()
    {
        return $this->is_hazardous;
    }

    /**
     * @param $is_hazardous
     * @return NeoDTO
     */
    public function setIsHazardous($is_hazardous): self
    {
        $this->is_hazardous = $is_hazardous;

        return $this;
    }
}
