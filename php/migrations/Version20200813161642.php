<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200813161642 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE neos ADD reference_id INT NOT NULL COMMENT \'Reference to NEO (reference_id)\', CHANGE speed speed DOUBLE PRECISION NOT NULL COMMENT \'Speed of NEO in km\\\\h\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE neos DROP reference_id, CHANGE speed speed INT NOT NULL COMMENT \'Speed of NEO in km\\\\h\'');
    }
}
